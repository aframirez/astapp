/**
 * Created by aframirez on 31/03/2015.
 */
module ModelsDefinitions {
    export enum StatusOptions {
        Open = 1,
        Close = 0,
        Pending = 2
    }

    export interface Location {
        latitude: string;
        longitude: string;
    }

    export interface User {
        userName: string;
        password: string;
        location: Location;
    }

    export interface Issue {
        id: number;
        status: StatusOptions;
        client:Client;
        assignee: User;
        assignationDate: Date;
        creationDate: Date;
        dateLastModification: Date;
        info:string;
        comments: string;
        color: string;
    }

    export interface Client {
        name:string;
        address:string;
        location: Location;
    }

}