///<reference path="entities.ts" />
//angular.module('ast.controllers',[]);

angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova', 'uiGmapgoogle-maps', 'services'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                //StatusBar.styleDefault();
            }
        });
    })


    .config(function($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider) {
        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'AppCtrl'
            })

            .state('app.search', {
                url: "/search",
                views: {
                    'menuContent': {
                        templateUrl: "templates/search.html"
                    }
                }
            })

            .state('app.browse', {
                url: "/browse",
                views: {
                    'menuContent': {
                        templateUrl: "templates/browse.html"
                    }
                }
            })

            .state('app.issues', {
                url: "/issues",
                views: {
                    'menuContent': {
                        templateUrl: "templates/issues.html",
                        controller: 'issuesController as Myissues'
                    }
                },
                resolve:{
                    //item: ['IssuesService', function(serv) {return serv.getData(); }]
                    item: ['IssuesService', function(serv) {
                       return serv.load();
                    }]
                }
            })

            .state('app.issue-detail', {
                url: "/issues/:id",
                views: {
                    'menuContent': {
                        templateUrl: "templates/issue-detail.html",
                        controller: 'issuesDetailController as midetail'
                    }
                },
                resolve:{
                    //item: ['IssuesService', '$stateParams', function(serv, $stateParams) {return serv.getIssue($stateParams.id); }]
                    item: ['IssuesService', '$stateParams', function(serv, $stateParams) {return serv.loadId($stateParams.id); }]

                }
            })

            .state('app.login', {
                url: "/login",
                views: {
                    'menuContent': {
                        templateUrl: "templates/login.html",
                        controller: 'PlaylistsCtrl'
                    }
                }
            })

            .state('app.cliente', {
                url: "/cliente",
                views: {
                    'menuContent': {
                        templateUrl: "templates/cliente.html",
                        controller: 'PlaylistsCtrl'
                    },
                    resolve: {
                        getClientes: function(){
                            var user: ModelsDefinitions.User
                            var ubicacion: ModelsDefinitions.Location;

                        }
                    }
                }
            })

            .state('app.acordeon', {
                url: "/acordeon",
                views: {
                    'menuContent': {
                        templateUrl: "templates/acordeon.html",
                        controller: 'acordeonController'
                    }
                }
            })

            .state('app.mapa', {
                url: "/mapa",
                views: {
                    'menuContent': {
                        templateUrl: "templates/mapa.html",
                        controller: 'mapaController as mimapa'
                    }
                }
            })

            .state('app.dashboard', {
                url: "/dashboard",
                views: {
                    'menuContent': {
                        templateUrl: "templates/dashboard.html",
                        controller: 'dashboardController as Mydash'
                    }
                },
                resolve:{
                    //item: ['IssuesService', function(serv) {return serv.getData(); }]
                    item: ['IssuesService', function(serv) {
                        return serv.load();
                    }]
                }
            })



            .state('app.single', {
                url: "/playlists/:playlistId",
                views: {
                    'menuContent': {
                        templateUrl: "templates/playlist.html",
                        controller: 'PlaylistCtrl'
                    }
                }
            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/login');

        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyCIzflz6CTcZn4OwE2NI-EFhbwIoqXjfes',
            v: '3.17',
            libraries: 'weather,geometry,visualization'
        });

    });
