///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities.ts" />

module ASTControllers {
    'use strict';

    class issuesController {
        
        data: Array<ModelsDefinitions.Issue>;
        filtro: string;
        opciones: any;

        static $inject = ['$filter','item'];
        constructor(private $filter, item) {
            //Controller Body
            this.filtro='Fecha';
            this.opciones = ModelsDefinitions.StatusOptions;
            this.data=item.data;

            //alert('Issues controller: ' +item);

            //var cliente={name:'Andres Felipe RM',address:'Cra 8 bis # 11-33',location: {latitude:'',longitude:''}};
            //var usuario={userName: '',password:'',location:{latitude:'',longitude:''}};
            //var estado:ModelsDefinitions.StatusOptions;
            //var estado2:ModelsDefinitions.StatusOptions;
            //var estado3:ModelsDefinitions.StatusOptions;
            //estado=ModelsDefinitions.StatusOptions.Open;
            //estado2=ModelsDefinitions.StatusOptions.Close;
            //estado3=ModelsDefinitions.StatusOptions.Pending;
            //var fecha:Date;
            //var fecha2:Date;
            //var fecha3:Date;
            //fecha=new Date('02/03/2015');
            //fecha2=new Date('11/09/2014');
            //fecha3=new Date('01/01/2015');
            //
            //var issue:ModelsDefinitions.Issue;
            //
            //issue = {
            //    id:1,
            //    status:estado,
            //    client:cliente,
            //    assignee: usuario,
            //    assignationDate:fecha,
            //    creationDate:fecha,
            //    dateLastModification:fecha,
            //    info:'Issue #1',
            //    comments:'list-red',
            //    color:''};
            //
            //var issue2:ModelsDefinitions.Issue;
            //
            //issue2 = {
            //    id:2,
            //    status:estado2,
            //    client:cliente,
            //    assignee: usuario,
            //    assignationDate:fecha2,
            //    creationDate:fecha2,
            //    dateLastModification:fecha2,
            //    info:'Issue #2',
            //    comments:'list-red',
            //    color:''};
            //
            //var issue3:ModelsDefinitions.Issue;
            //issue3 = {
            //    id:3,
            //    status:estado3,
            //    client:cliente,
            //    assignee: usuario,
            //    assignationDate:fecha3,
            //    creationDate:fecha3,
            //    dateLastModification:fecha3,
            //    info:'Issue #3',
            //    comments:'list-red',
            //    color:''};
            //
            //this.data = [issue, issue2, issue3];
            //this.data.push(issue);
            //this.data.push(issue);

            
        }

        ordenar(){
            //alert(this.filtro);
            var orderBy = this.$filter('orderBy');
            var filtrarpor='';

            if (this.filtro=='Fecha'){
                filtrarpor='assignationDate';
            }

            if (this.filtro=='Estado'){
                //alert('2 : ' + this.filtro);
                filtrarpor='status';
            }

            //alert('Se va ordenar por: ' + filtrarpor);
            this.data = orderBy(this.data, filtrarpor, false);
        }
    }

    angular.module('starter')
        .controller('issuesController', issuesController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/',
//                        templateUrl: 'templates/-template.html',
//                        controller: 'AST.controllers.issuesController as '
//                    });

//For use inside template:
//    {{.data}}

//Check dependencies inside app.ts
//    angular.module('AST.controllers', []);
//    angular.module('app', ['ionic', 'AST.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/controller.js"></script>