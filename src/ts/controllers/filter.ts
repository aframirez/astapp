///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />


module MyFilter {

    export interface Ifilter {
        getData():string[];
        getDataIndex(index:number):string;
        putData: (index:number, value:string) => void;
    }

    issuesFilter.$inject = [];
    function issuesFilter ()   {


        return function(input){
            var filtrado=[];
            var n=input.length;
            var i=0;

            for (i=0;i<n;i++){
                if (parseInt(input[i].status) !== ModelsDefinitions.StatusOptions.Close){
                    filtrado.push(input[i]);
                }
            }
            return filtrado;
        };
    }

    angular.module('starter')
        .filter('issuesFilter', issuesFilter);
}