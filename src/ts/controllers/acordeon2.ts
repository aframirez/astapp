///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities.ts" />

module ASTControllers {
    'use strict';

    class acordeonController {

        data: Array<ModelsDefinitions.Issue>;
        filtro: string;
        opciones: any;

        static $inject = ['$scope'];
        constructor(private $scope) {
            //Controller Body
            $scope.groups = [];

            $scope.groups = [
                { name: 'Pendientes', id: 1, items: [{ subName: 'SubBubbles1', subId: 'Issue #1' }, { subName: 'SubBubbles2', subId: 'Issue #2' }]},
                { name: 'Abiertos', id: 1, items: [{ subName: 'SubGrup1', subId: 'Issue #3' }, { subName: 'SubGrup1', subId: 'Issue #4' }]},
                { name: 'Cerrados', id: 1, items: [{ subName: 'SubGrup1', subId: 'Issue #5' }, { subName: 'SubGrup1', subId: 'Issue #6' }]},
            ];

            $scope.toggleGroup = function(group) {
                if ($scope.isGroupShown(group)) {
                    $scope.shownGroup = null;
                } else {
                    $scope.shownGroup = group;
                }
            };
            $scope.isGroupShown = function(group) {
                return $scope.shownGroup === group;
            };
        }

        //private toggleGroup(group){
        //    if (this.$scope.isGroupShown(group)) {
        //        this.$scope.shownGroup = null;
        //    } else {
        //        this.$scope.shownGroup = group;
        //    }
        //}
        //
        //private isGroupShown(group){
        //    return this.$scope.shownGroup === group;
        //}


    }

    angular.module('starter')
        .controller('acordeonController', acordeonController);
}
