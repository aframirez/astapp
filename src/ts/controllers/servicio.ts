/**
 * Created by aframirez on 06/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />


module ASTServices {
    export interface IissuesService {
        getData():string[];
    }
    class IssuesService implements IissuesService {

        data: any[];
        static $inject = ['$http','$q'];
        constructor(private $http, private $q) {

        }

        getData() {
            //alert(this.data[1].comments + ' segundo');
            //alert(this.data);
            return this.data;
        }

        getIssue(idIssue) {
            for (var i = 0; i < this.data.length; i++) {
                if ( parseInt(this.data[i].id, 10)  === parseInt(idIssue, 10)) {
                    return this.data[i];
                }
            }
        }

        load(){
            var self=this;

            return this.$http.get('http://186.117.158.20:443/api/Issues').
                success(function(data, status, headers, config) {

                    //self.data=data;
                    var issues = data;
                    return issues;
                    //alert(data[1].comments + ' primero');

                }).
                error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    alert("fallo la carga del archivo");
                });
        }

        loadId(idIssue){
            var self=this;

            return this.$http.get('http://186.117.158.20:443/api/Issues/' + idIssue).
                success(function(data, status, headers, config) {

                    //self.data=data;
                    var issues = data;
                    return issues;
                    //alert(data[1].comments + ' primero');

                }).
                error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    alert("fallo la carga del archivo");
                });

        }

    }

    angular.module('services', [])
        .service('IssuesService', IssuesService);

}
