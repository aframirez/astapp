/**
 * Created by aframirez on 06/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities.ts" />

module ASTControllers {
    'use strict';

    class issuesDetailController {

        data:Array<ModelsDefinitions.Issue>;
        opciones: any;
        datos: string[];

        static $inject = ['item','$http'];

        constructor(item, private $http) {
            //Controller Body
            this.datos = ['valor 1', 'valor 2'];
            this.opciones = ModelsDefinitions.StatusOptions;
            this.data = item.data;

        }

        aceptar(){

            alert('Mensaje');

            var miusuario:ModelsDefinitions.User;
            var where:string;
            miusuario={
                "userName": "CAMILO ANTONIO PEREZ SUAREZ",
                "password": "12345",
                "location": {
                    "longitude": "-70.6997712",
                    "latitude": "2.804312"
                }
            };

            where='55257d05b3299bb8552a89ea';

            var promesa=this.update(miusuario,where);
            promesa.success(function(){
                //document.getElementById("mensaje").innerHTML = "USUARIO REGISTRADO";
                alert('Actualizado correctamente');
            });

            promesa.error(function(){
                //document.getElementById("mensaje").innerHTML = "USUARIO NO REGISTRADO";
                alert('Error');
            })

        }

        update(datos,idactualizar){
            return this.$http.put('http://186.117.158.20:443/api/MyUsers/' + idactualizar , datos).
                success(function(data, status, headers, config) {

                    alert("actualizado");

                }).
                error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    alert("fallo la actualizacion");
                });
        }

    }

    angular.module('starter')
        .controller('issuesDetailController', issuesDetailController);
}

