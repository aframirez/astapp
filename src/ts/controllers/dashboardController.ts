///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module ASTControllers {
    'use strict';
    
    class dashboardController {

        data: Array<ModelsDefinitions.Issue>;
        numeroPendientes: number;
        opciones: any;
        fecha: Date;
            
        static $inject = ['item'];
        constructor(item) {
            //Controller Body
            this.numeroPendientes=0;
            this.opciones = ModelsDefinitions.StatusOptions;
            this.data=item.data;
            this.fecha=new Date('02/03/2015');
            this.calcularAlerta();
            this.calcularPendientes();
            
        }

        restarFechas(fecha1: number, fecha2: number) : number {
            return (fecha2 - fecha1) / 1000 / 60 / 60;
        }
        calcularAlerta() : void {
            var i : number;
            var resta : number;
            for (i = 0; i < this.data.length; i++) {

                //var fecha1:Date;
                //fecha1 = new Date(Date.parse(this.data[i].assignationDate.toDateString()));
                //alert(fecha1.getTime());




                resta = Math.abs(this.restarFechas(new Date(this.data[i].assignationDate.toString()).getTime(), new Date().getTime()));
                if (resta > 24 && this.data[i].status === ModelsDefinitions.StatusOptions.Open) {
                    this.data[i].color = 'list-red';
                }
                if (resta <= 24 && this.data[i].status === ModelsDefinitions.StatusOptions.Open) {
                    this.data[i].color = 'list-yellow';
                }
                if (resta > 24 && this.data[i].status === ModelsDefinitions.StatusOptions.Pending) {
                    this.data[i].color = 'list-purple';
                }
                if (resta <= 24 && this.data[i].status === ModelsDefinitions.StatusOptions.Pending) {
                    this.data[i].color = 'list-orange';
                }
            }
        }

        calcularPendientes(): void{
            var i : number;
            for (i = 0; i < this.data.length; i++) {
                if (this.data[i].status !== ModelsDefinitions.StatusOptions.Close){
                    this.numeroPendientes = this.numeroPendientes + 1;
                }
            }
        }

    }

    angular.module('starter')
        .controller('dashboardController', dashboardController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/',
//                        templateUrl: 'templates/-template.html',
//                        controller: 'AST.controllers.dashboardController as '
//                    });

//For use inside template:
//    {{.data}}

//Check dependencies inside app.ts
//    angular.module('AST.controllers', []);
//    angular.module('app', ['ionic', 'AST.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/controller.js"></script>