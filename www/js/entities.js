/**
 * Created by aframirez on 31/03/2015.
 */
var ModelsDefinitions;
(function (ModelsDefinitions) {
    (function (StatusOptions) {
        StatusOptions[StatusOptions["Open"] = 1] = "Open";
        StatusOptions[StatusOptions["Close"] = 0] = "Close";
        StatusOptions[StatusOptions["Pending"] = 2] = "Pending";
    })(ModelsDefinitions.StatusOptions || (ModelsDefinitions.StatusOptions = {}));
    var StatusOptions = ModelsDefinitions.StatusOptions;
})(ModelsDefinitions || (ModelsDefinitions = {}));

//# sourceMappingURL=entities.js.map