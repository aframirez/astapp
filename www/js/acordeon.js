/**
 * Created by aframirez on 08/04/2015.
 */
angular.module('starter')

    .controller('MyCtrl', function($scope) {
        $scope.groups = [];

        $scope.groups = [
            { name: 'Pendientes', id: 1, items: [{ subName: 'SubBubbles1', subId: 'Issue #1' }, { subName: 'SubBubbles2', subId: 'Issue #2' }]},
            { name: 'Abiertos', id: 1, items: [{ subName: 'SubGrup1', subId: 'Issue #3' }, { subName: 'SubGrup1', subId: 'Issue #4' }]},
            { name: 'Cerrados', id: 1, items: [{ subName: 'SubGrup1', subId: 'Issue #5' }, { subName: 'SubGrup1', subId: 'Issue #6' }]},
        ];


        /*
         * if given group is the selected group, deselect it
         * else, select the given group
         */
        $scope.toggleGroup = function(group) {
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
        };
        $scope.isGroupShown = function(group) {
            return $scope.shownGroup === group;
        };

    });
