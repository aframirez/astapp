///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var MyFilter;
(function (MyFilter) {
    issuesFilter.$inject = [];
    function issuesFilter() {
        return function (input) {
            var filtrado = [];
            var n = input.length;
            var i = 0;
            for (i = 0; i < n; i++) {
                if (parseInt(input[i].status) !== 0 /* Close */) {
                    filtrado.push(input[i]);
                }
            }
            return filtrado;
        };
    }
    angular.module('starter').filter('issuesFilter', issuesFilter);
})(MyFilter || (MyFilter = {}));

//# sourceMappingURL=../controllers/filter.js.map