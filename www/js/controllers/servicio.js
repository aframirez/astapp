/**
 * Created by aframirez on 06/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ASTServices;
(function (ASTServices) {
    var IssuesService = (function () {
        function IssuesService($http, $q) {
            this.$http = $http;
            this.$q = $q;
        }
        IssuesService.prototype.getData = function () {
            //alert(this.data[1].comments + ' segundo');
            //alert(this.data);
            return this.data;
        };
        IssuesService.prototype.getIssue = function (idIssue) {
            for (var i = 0; i < this.data.length; i++) {
                if (parseInt(this.data[i].id, 10) === parseInt(idIssue, 10)) {
                    return this.data[i];
                }
            }
        };
        IssuesService.prototype.load = function () {
            var self = this;
            return this.$http.get('http://186.117.158.20:443/api/Issues').success(function (data, status, headers, config) {
                //self.data=data;
                var issues = data;
                return issues;
                //alert(data[1].comments + ' primero');
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("fallo la carga del archivo");
            });
        };
        IssuesService.prototype.loadId = function (idIssue) {
            var self = this;
            return this.$http.get('http://186.117.158.20:443/api/Issues/' + idIssue).success(function (data, status, headers, config) {
                //self.data=data;
                var issues = data;
                return issues;
                //alert(data[1].comments + ' primero');
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("fallo la carga del archivo");
            });
        };
        IssuesService.$inject = ['$http', '$q'];
        return IssuesService;
    })();
    angular.module('services', []).service('IssuesService', IssuesService);
})(ASTServices || (ASTServices = {}));

//# sourceMappingURL=../controllers/servicio.js.map