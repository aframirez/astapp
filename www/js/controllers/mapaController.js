///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var astControllers;
(function (astControllers) {
    'use strict';
    var mapaController = (function () {
        function mapaController($ionicPlatform, $cordovaGeolocation, uiGmapGoogleMapApi, $http) {
            this.$ionicPlatform = $ionicPlatform;
            this.$cordovaGeolocation = $cordovaGeolocation;
            this.$http = $http;
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.lat = 4.805569;
            this.lon = -75.690299;
            this.distancia = 0;
            //var self=this;
            this.map = { center: { latitude: this.lat, longitude: this.lon }, zoom: 13 };
            this.marker = {
                id: 0,
                coords: {
                    latitude: 4.805569,
                    longitude: -75.690299
                } //},
            };
            this.obtenerPosicion();
        }
        mapaController.prototype.obtenerPosicion = function () {
            var self = this;
            self.geolocation = false;
            if (navigator.geolocation) {
                self.geolocation = navigator.geolocation;
            }
            self.$ionicPlatform.ready(function () {
                var posOptions = { timeout: 10000, enableHighAccuracy: false };
                self.$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
                    self.lat = position.coords.latitude;
                    self.lon = position.coords.longitude;
                }, function (err) {
                    alert(err.message);
                });
            });
        };
        mapaController.prototype.aceptar = function () {
            var self2 = this;
            setInterval(function () {
                self2.obtenerPosicion();
                self2.marker.coords.longitude = self2.lon;
                self2.marker.coords.latitude = self2.lat;
                //alert('lat:' + this.lat + ' lon:' + this.lon);
                var miusuario;
                var where;
                miusuario = {
                    "userName": "CAMILO ANTONIO PEREZ SUAREZ",
                    "password": "12345",
                    "location": {
                        "longitude": self2.lon.toString(),
                        "latitude": self2.lat.toString()
                    }
                };
                where = '55257d05b3299bb8552a89ea';
                var promesa = self2.update(miusuario, where);
                promesa.success(function () {
                    //document.getElementById("mensaje").innerHTML = "USUARIO REGISTRADO";
                    alert('Posicion actualizada correctamente');
                });
                promesa.error(function () {
                    //document.getElementById("mensaje").innerHTML = "USUARIO NO REGISTRADO";
                    alert('Error');
                });
            }, 30000);
        };
        //
        //cicloActualizar(self2:any){
        //    self2.obtenerPosicion();
        //
        //    //alert('lat:' + this.lat + ' lon:' + this.lon);
        //    var miusuario:ModelsDefinitions.User;
        //    var where:string;
        //    miusuario={
        //        "userName": "CAMILO ANTONIO PEREZ SUAREZ",
        //        "password": "12345",
        //        "location": {
        //            "longitude": this.lon.toString(),
        //            "latitude":  this.lat.toString()
        //        }
        //    };
        //
        //    where='55257d05b3299bb8552a89ea';
        //
        //    var promesa=this.update(miusuario,where);
        //    promesa.success(function(){
        //        //document.getElementById("mensaje").innerHTML = "USUARIO REGISTRADO";
        //        //alert('Actualizado correctamente');
        //    });
        //
        //    promesa.error(function(){
        //        //document.getElementById("mensaje").innerHTML = "USUARIO NO REGISTRADO";
        //        alert('Error');
        //    })
        //}
        mapaController.prototype.update = function (datos, idactualizar) {
            return this.$http.put('http://186.117.158.20:443/api/MyUsers/' + idactualizar, datos).success(function (data, status, headers, config) {
                //alert("actualizado");
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("fallo la actualizacion");
            });
        };
        mapaController.$inject = ['$ionicPlatform', '$cordovaGeolocation', 'uiGmapGoogleMapApi', '$http'];
        return mapaController;
    })();
    angular.module('starter').controller('mapaController', mapaController);
})(astControllers || (astControllers = {}));
//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/',
//                        templateUrl: 'templates/-template.html',
//                        controller: 'ast.controllers.mapaController as '
//                    });
//For use inside template:
//    {{.data}}
//Check dependencies inside app.ts
//    angular.module('ast.controllers', []);
//    angular.module('app', ['ionic', 'ast.controllers'])
//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/controller.js"></script> 

//# sourceMappingURL=../controllers/mapaController.js.map