///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ASTControllers;
(function (ASTControllers) {
    'use strict';
    var dashboardController = (function () {
        function dashboardController(item) {
            //Controller Body
            this.numeroPendientes = 0;
            this.opciones = ModelsDefinitions.StatusOptions;
            this.data = item.data;
            this.fecha = new Date('02/03/2015');
            this.calcularAlerta();
            this.calcularPendientes();
        }
        dashboardController.prototype.restarFechas = function (fecha1, fecha2) {
            return (fecha2 - fecha1) / 1000 / 60 / 60;
        };
        dashboardController.prototype.calcularAlerta = function () {
            var i;
            var resta;
            for (i = 0; i < this.data.length; i++) {
                //var fecha1:Date;
                //fecha1 = new Date(Date.parse(this.data[i].assignationDate.toDateString()));
                //alert(fecha1.getTime());
                resta = Math.abs(this.restarFechas(new Date(this.data[i].assignationDate.toString()).getTime(), new Date().getTime()));
                if (resta > 24 && this.data[i].status === 1 /* Open */) {
                    this.data[i].color = 'list-red';
                }
                if (resta <= 24 && this.data[i].status === 1 /* Open */) {
                    this.data[i].color = 'list-yellow';
                }
                if (resta > 24 && this.data[i].status === 2 /* Pending */) {
                    this.data[i].color = 'list-purple';
                }
                if (resta <= 24 && this.data[i].status === 2 /* Pending */) {
                    this.data[i].color = 'list-orange';
                }
            }
        };
        dashboardController.prototype.calcularPendientes = function () {
            var i;
            for (i = 0; i < this.data.length; i++) {
                if (this.data[i].status !== 0 /* Close */) {
                    this.numeroPendientes = this.numeroPendientes + 1;
                }
            }
        };
        dashboardController.$inject = ['item'];
        return dashboardController;
    })();
    angular.module('starter').controller('dashboardController', dashboardController);
})(ASTControllers || (ASTControllers = {}));
//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/',
//                        templateUrl: 'templates/-template.html',
//                        controller: 'AST.controllers.dashboardController as '
//                    });
//For use inside template:
//    {{.data}}
//Check dependencies inside app.ts
//    angular.module('AST.controllers', []);
//    angular.module('app', ['ionic', 'AST.controllers'])
//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/controller.js"></script> 

//# sourceMappingURL=../controllers/dashboardController.js.map